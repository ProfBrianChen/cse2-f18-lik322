//prompts the user for the dimensions of a pyramid and returns the volume inside the pyramid.
import java.util.Scanner;
public class Pyramid
{
  public static void main(String[] args) 
  {
    Scanner myScanner = new Scanner( System.in );
    System.out.print("The square side of the pyramid is (input length):");
    double length = myScanner.nextDouble();
    System.out.print("The height of the pyramid is (input height):");
    double height = myScanner.nextDouble();
    //get data from user by Scanner
    double volume;
    volume = Math.pow(length,2)*height/3;
    //calculation formula V =h*A*1/3
    int volumefixed =(int) volume;
    //fix the volume into integer 
    System.out.println("The volume inside the pyramid is:"+volumefixed+".");
  }  //end of main method   
} //end of class