//asks the user for the number of acres of land affected by hurricane precipitation and how many inches of rain were dropped on average.  
//Convert the quantity of rain into cubic miles.
import java.util.Scanner;
public class Convert
{
  public static void main(String[] args) 
  {
    Scanner myScanner = new Scanner (System.in);
    System.out.print("Enter the affected area in acres: ");
    double area = myScanner.nextDouble();
    System.out.print("Enter the rainfall in the affected area: ");
    double height = myScanner.nextDouble();
    //asks the user for the number of acres of land affected by hurricane precipitation and how many inches of rain were dropped on average
    //640 acre =1 Squaremile  63360 inch = 1 mile 
    double areaInSquaremile = area * (0.0015625);
    double heightInMile = height * (0.0000157828);
    double volume = areaInSquaremile*heightInMile;
    //calcultion
    int volumefixed = (int) (volume*100000000);
    double volumefinal = (double)volumefixed/100000000;
    //fix the volume to 8 digits in decimal place as the guide shows
    System.out.println("the quantity of rain in cubic miles is " + volumefinal+ ".");
  }  //end of main method   
} //end of class 