//CSE2 lik322 Lingtao Kong

import java.util.Scanner;
public class RemoveElements
{
 public static void main(String [] arg)
  {
	  Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
	  String answer="";
  	do
    {
  	  System.out.print("Random input 10 ints [0-9]");
  	  num = randomInput();
  	  String out = "The original array is:";
    	out += listArray(num);
  	  System.out.println(out);
    	System.out.print("Enter the index ");
      index = scan.nextInt();
  	  newArray1 = delete(num,index);
  	  String out1="The output array is ";
  	  out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	  System.out.println(out1);
      System.out.print("Enter the target value ");
    	target = scan.nextInt();
    	newArray2 = remove(num,target);
  	  String out2="The output array is ";
  	  out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	  System.out.println(out2);
  	  System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	  answer=scan.next();
	  }
    while(answer.equals("Y") || answer.equals("y"));
 }
  //main method
  public static String listArray(int num[])
  {
	String out="{";
	for(int j=0;j<num.length;j++)
  {
  	if(j>0)
    {
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  //this method list the array
 public static int[] randomInput()
  {
    int num[]=new int[10];
    for(int i=0;i<9;i++)
    {
      int random =(int) (Math.random()*10);
      num [i]=random;
    }
   return num;
  }
  //The randomInput() method generates an array of 10 random integers between 0 to 9.  
  //Implement randomInput so that it fills the array with random integers and returns the filled array. 
 public static int[] delete(int[]num,int index)
 {
   if(index>10||index<0)
   {
     System.out.println("The index is not valid.");
     return num;
   }
   else
   {
     int fix[]=new int[num.length-1];
     for(int i=0;i<index-1;i++)
      {
        fix[i]=num[i];
      }
     for(int i=index-1;i<fix.length-1;i++)
     {
       fix[i]=num[i+1];
     } 
     System.out.println("Index "+ index +" element is removed");
     return fix;
   }
  }
  //The method takes, as input, an integer array called list and an integer called pos.  
  //It should create a new array that has one member fewer than list, and be composed of all of the same members except the member in the position pos.  
 public static int[] remove(int[]num,int target)
   { 
   int k = 0;
   for(int i=0;i<num.length;i++)
    {
     if(num[i] == target)
     {
       k++;
     }
   }
   if(k>0)
   {
     System.out.println("Element "+target+" has been found");
   }
   else
   {
     System.out.println("Element "+target+" has not been found");
   }
   int fix[]=new int[num.length-k];
   int m =0;
   for(int i=0;i<num.length-m;i++)
    {
     if (num[i] !=target)
     {
       fix[i-m] =num[i];
     }
     if(num[i] == target)
     {
       m++;
     }
    } 
   return fix;
  }
  //Method remove(list,target) deletes all the elements that are equal to target, returning a new list without all those new elements.
}
