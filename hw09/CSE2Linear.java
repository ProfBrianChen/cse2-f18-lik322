//CSE2 lik322 Lingtao Kong
//prompts the user to enter 15 ints for students final grades in CSE2.
// scramble the sorted array randomly, and print out the scrambled array. Prompt the user again to enter an int to be searched for, and use linear search to find the grade. 
import java.util.Scanner;
import java.util.Random;
public class CSE2Linear
{
  public static int[] scramble(int grades[])
  {
    System.out.println("Scrambled");
    int[] scrambled = new int[15]; 
    for(int i=0;i<15;i++)
    {
      int random =(int) (Math.random()*(15-i));
      scrambled[i]=grades[random];
      grades[random] = grades[14-i]; 
      System.out.print(scrambled[i]+" ");  
    }
    System.out.println("");
    return scrambled;    
  }
  // this method scramble the grades
  public static void binary (int grades[],int i)
  {
    int count =0;
    boolean result = false;
    int high =15;
    int low =0;
    int mid;
    while(low<=high)
    {
      mid = (low+high)/2;
      if (grades[mid]==i)
      {
        result = true;
        count++;
        break;
      }
      if(grades[mid]>i)
      {
        high= mid-1;
        count++;
      }
      if(grades[mid]<i)
      {
        low= mid+1;
        count++;
      }
    }
    if(result==true)
    {
      System.out.println(i+" was found in the list with "+count+" interactions");
    }
    if(result==false)
    {
      System.out.println(i+" was not found in the list with"+count+" interactions");
    }
  }
  //this method check the result to find if there is a certain score
  public static void linear (int grades[],int j)
  {
    boolean result = false;
    for(int i=0;i<15;i++)
    {
       if(grades[i]==j)
       {
         
         result = true;
         break;
       }
    }
      if(!result)
      {
        System.out.println(j+" was not found in the list");
      }
      else
      {
      System.out.println(j+" was found in the list");
      }
  }
  //this method check the result to find if there is a certain score
  public static void main(String[] args) 
  {
    Scanner myScanner = new Scanner( System.in );
        int lastnum = 0;
        System.out.print("Enter 15 ascending ints for final grades in CSE2: ");//ask for input
        System.out.println("");//format
        int[] grades = new int[15];
        for (int k=0 ; k<15 ;k++)
        {          
          boolean noerror = true;//check the bad inputs
           if (myScanner.hasNextInt()==false)
           {
            System.out.println("You must input and integer");
            noerror=false;
           }
           if(!noerror)
           {
            break;
           }     
           grades[k] = myScanner.nextInt();
           if (grades[k]>100||grades[k]<0)
           {
            System.out.println("the grade must be from 0-100");
            noerror = false;
           }
           
           if(lastnum > grades[k])
           {
             System.out.println("the grade must be ascending");
             noerror = false;
           }
          lastnum = grades[k];          
          if(!noerror)
          {
            break;
          }     
       }
      System.out.println("Enter a grade to search for:");
      int i = myScanner.nextInt();
      binary(grades,i);
      scramble(grades);
      System.out.println("Enter a grade to search for:");
      int j = myScanner.nextInt();
      linear(grades,j);
    }  
  //main method
}