// My bicycle cyclometer records two kinds of data, the time elapsed in seconds, and the number of rotations of the front wheel during that time. 
//For two trips,given time and rotation count, the program will print the number of minutes and counts for each trip.
//print the distance of each trip in miles and two trips combined
public class Cyclometer 
{
    	// main method required for every Java program
   	public static void main(String[] args) 
  {
    int secsTrip1=480;  // time taken for trip 1
    int secsTrip2=3220;  // time taken for trip 2
	  int countsTrip1=1561;  //the counts for trip 1
  	int countsTrip2=9037; //the counts for trip 2
    double wheelDiameter=27.0,  // constant used to calculate the length
  	PI=3.14159, // constant pi
  	feetPerMile=5280,  // constant
  	inchesPerFoot=12,   //cosnstant
  	secondsPerMinute=60;  //constant
	  double distanceTrip1, distanceTrip2,totalDistance;  // double the sonstants
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	  totalDistance=distanceTrip1+distanceTrip2;
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");
	}  //end of main method   
} //end of class