import java.util.Scanner;
public class PatternB
{
  public static void main(String[] args) 
  {
    Scanner myScanner = new Scanner( System.in );
    System.out.print("int a number from 1-10 which is the length of the pyramid");  
    while(myScanner.hasNextInt()==false)
    {
      System.out.print("error,you need input of integer\r\n");
      myScanner.next();
      System.out.print("int a number from 1-10 which is the length of the pyramid");
    }//give error when bad ints happens
    int loops= myScanner.nextInt()+1;
    int i = loops; //number of rolls
     while (i<=loops&&i>0)
     { 
       int k= 1;
       while (k<i)
       {
       System.out.print(k);  
       k++;
       }
       System.out.println("");
     i--;
     }
  }  //end of main method   
} //end of class