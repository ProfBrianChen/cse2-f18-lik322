//Poker is a game of chance. In poker, players draw sets of five playing cards, called hands. Different hands have different probabilities of occurring. The objective of this homework is to use while loops to calculate these probabilities.  We will limit our hands to those related to face values including: Four-of-a-kind, Three-of-a-kind, Two-Pair, and One-pair. 
import java.util.Scanner;
public class PatternA
{
  public static void main(String[] args) 
  {
    Scanner myScanner = new Scanner( System.in );
    System.out.print("int a number from 1-10 which is the length of the pyramid");  
    while(myScanner.hasNextInt()==false)
    {
      System.out.print("error,you need input of integer\r\n");
      myScanner.next();
      System.out.print("int a number from 1-10 which is the length of the pyramid");
    }//give error when bad ints happens
    int loops= myScanner.nextInt();
    int i = 1; //number of rolls
     while (i<=loops)
     { 
       int k= 1;
       while (k<=i)
       {
       System.out.print(k);  
       k++;
       }
       System.out.println("");
     i++;
     }
  }  //end of main method   
} //end of class
