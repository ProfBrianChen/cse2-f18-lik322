//The program will bury the character in a handful of stars
import java.util.Scanner;
public class EncryptedX
{
  public static void main(String[] args) 
  {
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Give an integer between 0 - 100 as the size");//ask for input
    while(myScanner.hasNextInt()==false)
    {
      System.out.print("error,you need input of integer\r\n");
      myScanner.next();
      System.out.print("Give an integer between 0 - 100 as the size");
    }//give error when bad ints happens
    int size = myScanner.nextInt();//store the input
    int numofline = size+1;
       for(int i=1;i<=numofline;i++)//nested loop to create lines
       {
         for(int k=1;k<=numofline;k++)//nested loop to put "" or "*" in
         {
           if(k!=i&&k!=numofline+1-i)//for the statement not "" one print *
           System.out.print("*");
           else
           System.out.print(" ");
         }
         System.out.println("");//change line
       }
  }  //end of main method   
} //end of class


