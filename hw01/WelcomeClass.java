///CSE 002 HW
public class WelcomeClass
{
  public static void main(String args[])
  {
    //print welcomeinformation to termial window
    //\r\n stands for change the line
    System.out.println("  -----------\r\n" + 
    	 "  | WELCOME |\r\n" + 
    	 "  -----------\r\n" + 
     	 "  ^  ^  ^  ^  ^  ^\r\n" + 
     	 " / \\/ \\/ \\/ \\/ \\/ \\\r\n" + 
    	 "<-L--I--K--3--2--2->\r\n" + 
     	 " \\ /\\ /\\ /\\ /\\ /\\ /\r\n" + 
    	 "  v  v  v  v  v  v\r\n" );
  }
}