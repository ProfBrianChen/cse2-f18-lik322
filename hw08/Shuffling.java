//lik322 Lingtao Kong CSE2
//print out all the cards in the deck, shuffle the whole deck of cards, then print out the cards in the deck, all shuffled, then getting a hand of cards and print them out. 
import java.util.Scanner;
public class Shuffling
{ 
  public static void printArray(String deck[])
  {
    for (int i=0; i<26; i++)
    { 
      System.out.print(deck[i]+" "); 
    } 
    System.out.println("");
    for (int i=26; i<52; i++)
    { 
      System.out.print(deck[i]+" "); 
    }
    System.out.println("");
  }
  //print the deck
  public static void printarray(String deck[])
  {
    for (int i=0; i<5; i++)
    { 
      System.out.print(deck[i]+" "); 
    } 
  }
  //print the hand
  public static String[] shuffle(String deck[])
  {
    System.out.println("shuffled");
    String[] shuffled = new String[52]; 
    for(int i=0;i<52;i++)
    {
      int random =(int) (Math.random()*(52-i)+1);
      shuffled[i]=deck[random];
      deck[random] = deck[i];       
    }
    return shuffled;    
  }
  //shuffle the deck
  public static String[] getHand(String deck[],int index,int hands)
  { 
    System.out.println("hands");
    String[] handcard= new String[5];
    for(int i=0; i<hands; i++)
    {
       int random = (int)(index-i);
       handcard[i]= deck[random];
    }
    return handcard;
  }
  //get tge hand
  public static void main(String[] args) 
  { 
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    for (int i=0; i<52; i++)
    { 
        cards[i]=rankNames[i%13]+suitNames[i/13]; 
    } 
    System.out.println();
    printArray(cards); 
    cards = shuffle(cards); 
    printArray(cards); 
    while(again == 1)
    { 
      hand = getHand(cards,index,numCards); 
      printarray(hand);
      index = index - numCards;
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }  
  } 
}
