// The classic casino game, Craps, involves the rolling of two six sided dice and evaluating the result of the roll.  Among other things, craps is notable for producing slang terminology for describing the outcome of a roll of two dice.
//Ask the user if theyd like randomly cast dice or if theyd like to state the two dice they want to evaluate.
//If they want randomly cast dice, generate two random numbers from 1-6, inclusive, representing the outcome of the two dice cast in the game.
//If they want user provided dice, use the Scanner twice to ask the user for two integers from 1-6, inclusive.  Assume the user always provides an integer, but check if the integer is within the range.
//Determine the slang terminology of the outcome of the roll
//Print out the slang terminology
import java.util.Scanner;
public class CrapsIf
{
  public static void main(String[] args) 
  {
    Scanner myScanner = new Scanner (System.in);
    System.out.print("Would you like randomly cast dice or state the two dice you want to evaluate?\r\n"
                     +"Enter 1 if you want a randomly cast dice. Enter 2 if you want to evaluate your own number");
    //make user make the decision of ranom dice or enter themselves
    double state = myScanner.nextDouble();
    if (state == 1)
    {
      int Num1 = (int)(Math.random()*6+1);
      int Num2 = (int)(Math.random()*6+1);
      //get random number for the users
      if (Num1+ Num2 == 2)
      System.out.println("slang terminology is "+ "Snake Eyes");
      if (Num1+ Num2 == 3)
      System.out.println("slang terminology is "+ "Ace Deuce");
      if (Num1+ Num2 == 4 && Num1 != Num2)
      System.out.println("slang terminology is "+ "Easy Four");
      if (Num1+ Num2 == 4 && Num1 == Num2)
      System.out.println("slang terminology is "+ "Hard Four"); 
      if (Num1+ Num2 == 5)
      System.out.println("slang terminology is "+ "Fever five");
      if (Num1+ Num2 == 6 && Num1 != Num2)
      System.out.println("slang terminology is "+ "Easy six");
      if (Num1+ Num2 == 6 && Num1 == Num2)
      System.out.println("slang terminology is "+ "Hard six");
      if (Num1+ Num2 == 7)
      System.out.println("slang terminology is "+ "Seven out");
      if (Num1+ Num2 == 8 && Num1 != Num2)
      System.out.println("slang terminology is "+ "Easy eight");
      if (Num1+ Num2 == 8 && Num1 == Num2)
      System.out.println("slang terminology is "+ "Hard eight");
      if (Num1+ Num2 == 9)
      System.out.println("slang terminology is "+ "Nine");
      if (Num1+ Num2 == 10 && Num1 != Num2)
      System.out.println("slang terminology is "+ "Easy ten");
      if (Num1+ Num2 == 10 && Num1 == Num2)
      System.out.println("slang terminology is "+ "Hard ten");
      if (Num1+ Num2 == 11)
      System.out.println("slang terminology is "+ "Yo-leven");
      if (Num1+ Num2 == 12)
      System.out.println("slang terminology is "+ "Boxcars");
      //anlyse the number and come up with terminology
    }
    else if (state == 2)
    {
      System.out.print("What is your first number?(give an integer from 1-6)");
      double Num3 = myScanner.nextDouble();
      System.out.print("What is your second number?(give an integer from 1-6)");
      double Num4 = myScanner.nextDouble();
      //get number from users
      if (Num3 >= 7 || Num4 >= 7 ||Num3 <=0||Num4 <=0)
      // When the user give number that is out of range, give error
      System.out.println("error, please give an integer from 1-6");
      else if (Num3+ Num4 == 2)
      System.out.println("slang terminology is "+ "Snake Eyes");
      else if (Num3+ Num4 == 3)
      System.out.println("slang terminology is "+ "Ace Deuce");
      else if (Num3+ Num4 == 4 && Num3 != Num4)
      System.out.println("slang terminology is "+ "Easy Four");
      else if (Num3+ Num4 == 4 && Num3 == Num4)
      System.out.println("slang terminology is "+ "Hard Four"); 
      else if (Num3+ Num4 == 5)
      System.out.println("slang terminology is "+ "Fever five");
      else if (Num3+ Num4 == 6 && Num3 != Num4)
      System.out.println("slang terminology is "+ "Easy six");
      else if (Num3+ Num4 == 6 && Num3 == Num4)
      System.out.println("slang terminology is "+ "Hard six");
      else if (Num3+ Num4 == 7)
      System.out.println("slang terminology is "+ "Seven out");
      else if (Num3+ Num4 == 8 && Num3 != Num4)
      System.out.println("slang terminology is "+ "Easy eight");
      else if (Num3+ Num4 == 8 && Num3 == Num4)
      System.out.println("slang terminology is "+ "Hard eight");
      else if (Num3+ Num4 == 9)
      System.out.println("slang terminology is "+ "Nine");
      else if (Num3+ Num4 == 10 && Num3 != Num4)
      System.out.println("slang terminology is "+ "Easy ten");
      else if (Num3+ Num4 == 10 && Num3 == Num4)
      System.out.println("slang terminology is "+ "Hard ten");
      else if (Num3+ Num4 == 11)
      System.out.println("slang terminology is "+ "Yo-leven");
      else if (Num3+ Num4 == 12)
      System.out.println("slang terminology is "+ "Boxcars");
      else System.out.println("error, please give an integer from 1-6");
      //when users enter things other than number, give error
    }
    else
    {
      System.out.println("error");
    }
  }  //end of main method   
} //end of class 