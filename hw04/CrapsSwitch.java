// The classic casino game, Craps, involves the rolling of two six sided dice and evaluating the result of the roll.  Among other things, craps is notable for producing slang terminology for describing the outcome of a roll of two dice.
//Ask the user if theyd like randomly cast dice or if theyd like to state the two dice they want to evaluate.
//If they want randomly cast dice, generate two random numbers from 1-6, inclusive, representing the outcome of the two dice cast in the game.
//If they want user provided dice, use the Scanner twice to ask the user for two integers from 1-6, inclusive.  Assume the user always provides an integer, but check if the integer is within the range.
//Determine the slang terminology of the outcome of the roll
//Print out the slang terminology
import java.util.Scanner;
public class CrapsSwitch
{
 public static void main(String[] args) 
  {
   Scanner myScanner = new Scanner (System.in);
   System.out.print("Would you like randomly cast dice or state the two dice you want to evaluate?\r\n"
                   +"Enter 1 if you want a randomly cast dice. Enter 2 if you want to evaluate your own number");
   //make user make the decision of ranom dice or enter themselves
   double state = myScanner.nextDouble();
   int state1 = (int) state;
   switch (state1)
   {
     case(1):
      {
       int Num1 = (int)(Math.random()*6+1);
       int Num2 = (int)(Math.random()*6+1);
       //get random number for the users
       switch (Num1+Num2)
       {
        case (2):
        System.out.println("slang terminology is "+ "Snake Eyes");
        break;
        case (3):
        System.out.println("slang terminology is "+ "Ace Deuce");
        break;
        case (4):
        switch(Num1-Num2)
        {
            case(0):
            System.out.println("slang terminology is "+ "Hard Four");
            break;
            default:
            System.out.println("slang terminology is "+ "Easy Four");
            break;
        }
        break;
        case (5):
        System.out.println("slang terminology is "+ "Fever five");
        break;
        case (6):
        switch(Num1-Num2)
        {
            case(0):
            System.out.println("slang terminology is "+ "Hard six");
            break;
            default:
            System.out.println("slang terminology is "+ "Easy six");
            break;
        }
        break;
        case (7):
        System.out.println("slang terminology is "+ "Seven out");
        break;
        case(8):
        switch(Num1-Num2)
        {
            case(0):
            System.out.println("slang terminology is "+ "Hard eight");
            break;
            default:
            System.out.println("slang terminology is "+ "Easy eight");
            break;
        }
        break;
        case (9):
        System.out.println("slang terminology is "+ "Nine");
        break;
        case (10):
        switch(Num1-Num2)
        {
            case(0):
            System.out.println("slang terminology is "+ "Hard tem");
            break;
            default:
            System.out.println("slang terminology is "+ "Easy ten");
            break;
        }
        break;
        case (11):
        System.out.println("slang terminology is "+ "Yo-leven");
        break;
        case (12):
        System.out.println("slang terminology is "+ "Boxcars");
       }
      }break;
   case(2):
    {       
      System.out.print("What is your first number?(give an integer from 1-6)");
      double Num3 = myScanner.nextDouble();
      System.out.print("What is your second number?(give an integer from 1-6)");
      double Num4 = myScanner.nextDouble();
      //get number from users
      switch ((int)Num3+(int)Num4)
      {
        case (2):
        System.out.println("slang terminology is "+ "Snake Eyes");
        break;
        case (3):
        System.out.println("slang terminology is "+ "Ace Deuce");
        break;
        case (4):
        switch((int)Num3-(int)Num4)
        {
            case(0):
            System.out.println("slang terminology is "+ "Hard Four");
            break;
            default:
            System.out.println("slang terminology is "+ "Easy Four");
            break;
        }
        break;
        case (5):
        System.out.println("slang terminology is "+ "Fever five");
        break;
        case (6):
        switch((int)Num3-(int)Num4)
        {
            case(0):
            System.out.println("slang terminology is "+ "Hard six");
            break;
            default:
            System.out.println("slang terminology is "+ "Easy six");
            break;
        }
        break;
        case (7):
        System.out.println("slang terminology is "+ "Seven out");
        break;
        case (8):
        switch((int)Num3-(int)Num4)
        {
            case(0):
            System.out.println("slang terminology is "+ "Hard eight");
            break;
            default:
            System.out.println("slang terminology is "+ "Easy eight");
            break;
        }
        break;
        case (9):
        System.out.println("slang terminology is "+ "Nine");
        break;
        case (10):
        switch((int)Num3-(int)Num4)
        {
            case(0):
            System.out.println("slang terminology is "+ "Hard tem");
            break;
            default:
            System.out.println("slang terminology is "+ "Easy ten");
            break;
        }
        break;
        case (11):
        System.out.println("slang terminology is "+ "Yo-leven");
        break;
        case (12):
        System.out.println("slang terminology is "+ "Boxcars");
        break;
      //when users enter things other than number, give error
      }break;
    }
  }
 }//end of main method
}//end of class 