//
import java.util.Scanner;
public class Course
{
  public static void main(String[] args) 
  {
    Scanner myScanner = new Scanner( System.in );
    System.out.print("what's the course number");
    while(myScanner.hasNextInt()==false)
    {
      System.out.print("error,you need input of integer");
      myScanner.next();
    }
    Scanner myScanner1 = new Scanner( System.in );
    System.out.print("what's the department name");
    while(myScanner1.hasNext()==false)
    {
      System.out.print("error,you need input of string");
      myScanner1.next();
    }
    Scanner myScanner2 = new Scanner( System.in );
    System.out.print("what's the number of times it meets in a week");
    while(myScanner2.hasNextInt()==false)
    {
      System.out.print("error,you need input of integer");
      myScanner2.next();
    }
    Scanner myScanner3 = new Scanner( System.in );
    System.out.print("what's the time the class starts");
    while(myScanner3.hasNextDouble()==false)
    {
      System.out.print("error,you need input of double");
      myScanner3.next();
    }
    Scanner myScanner4 = new Scanner( System.in );
    System.out.print("what's the instructor name");
    while(myScanner4.hasNext()==false)
    {
      System.out.print("error,you need input of string");
      myScanner4.next();
    }
    Scanner myScanner5 = new Scanner( System.in );
    System.out.print("what's the number of students");
    while(myScanner5.hasNextInt()==false)
    {
      System.out.print("error,you need input of integer");
      myScanner5.next();
    }
 
  }  //end of main method   
} //end of class
