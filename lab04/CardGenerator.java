//
import java.math.*;
public class CardGenerator
{
  public static void main(String[] args) 
  {
    int Card = (int)(Math.random()*52+1);
    int number = 0;
    String color = ""; 
    String numberA = "";
    if (Card <= 13)
    {
      color = "diamonds";
      number = (int) Card;
    }
    else if (Card <= 26)
    {
     color = "clubs";
     number = (int)Card-13;
    } 
    else if (Card <= 39)
    {
      color = "hearts";
      number = (int)Card -26;
    }  
    else if (Card <=52)
    {
      color ="spades";
      number = (int) Card-39;
    }
    if (number == 1)
    {
      numberA = "Ace";
      System.out.println("You picked the " + numberA +" of "+ color);
    }
    else if (number == 11)
    {
      numberA = "Jack";
      System.out.println("You picked the " + numberA +" of "+ color);
    } 
    else if (number == 12)
    {
      numberA = "Queen"; 
      System.out.println("You picked the " + numberA +" of "+ color);
    }
    else if (number == 13)
    {
      numberA ="King";
      System.out.println("You picked the " + numberA +" of "+ color);
    }
    else
     System.out.println("You picked the " + number +" of "+ color);
    
  }  //end of main method   
} //end of class 