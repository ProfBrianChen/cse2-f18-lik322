// The program calcculates the following results with the tax rates number and price of items
//Total cost of each kind of item (i.e. total cost of pants, etc)
//Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
//Total cost of purchases (before tax)
//Total sales tax
//Total paid for this transaction, including sales tax. 

public class Arithmetic 
{
    	// main method required for every Java program
   	public static void main(String[] args) 
  {
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    //the tax rate
    double paSalesTax = 0.06;

    double costPants,taxPants,paidPants,costShirts,taxShirts,paidShirts,costBelts,taxBelts,paidBelts,totalCost,totalTax,totalPaid;
	  costPants = numPants*pantsPrice;
    taxPants = costPants*paSalesTax;
    paidPants = costPants+taxPants;
    costShirts = numShirts*shirtPrice;
    taxShirts = costShirts*paSalesTax;
    paidShirts = costShirts+taxShirts;
    costBelts =  numBelts*beltCost;
    taxBelts =  costBelts*paSalesTax;
    paidBelts = costBelts+taxBelts;
    totalCost = costBelts+costShirts+costPants;
    totalTax = taxPants+taxBelts+taxShirts;
    totalPaid = totalCost+totalTax;
    //calculation for the results.
    double taxPants1 = taxPants * 100;
    int taxPants2 = (int) taxPants1;
    double taxPants3 = taxPants2 /100.0;
    double paidPants1 = paidPants * 100;
    int paidPants2 = (int) paidPants1;
    double paidPants3 = paidPants2 /100.0;
    double taxShirts1 = taxShirts * 100;
    int taxShirts2 = (int) taxShirts1;
    double taxShirts3 = taxShirts2 /100.0;
    double paidShirts1 = paidShirts * 100;
    int paidShirts2 = (int) paidShirts1;
    double paidShirts3 = paidShirts2 /100.0;
    double taxBelts1 = taxBelts * 100;
    int taxBelts2 = (int) taxBelts1;
    double taxBelts3 = taxBelts2 /100.0;
    double paidBelts1 = paidBelts * 100;
    int paidBelts2 = (int) paidBelts1;
    double paidBelts3 = paidBelts2 /100.0;
    double totalTax1 = totalTax * 100;
    int totalTax2 = (int) totalTax1;
    double totalTax3 = totalTax2 /100.0;
    double totalPaid1 = totalPaid * 100;
    int totalPaid2 = (int) totalPaid1;
    double totalPaid3 = totalPaid2 /100.0;
    System.out.println("The Total cost of purchases for Pants (before tax)is "+costPants+". Sales tax charged buying Pants is "+ taxPants3+". Total cost of Pants is "+paidPants3);
	  System.out.println("The Total cost of purchases for Shirts (before tax) is "+costShirts+". Sales tax charged buying Pants is "+ taxShirts3 +". Total cost of Pants is "+ paidShirts3);
    System.out.println("The Total cost of purchases for Pants (before tax) is "+costBelts+". Sales tax charged buying Pants is "+ taxBelts3+". Total cost of Pants is "+ paidBelts3);
    System.out.println("The Total cost of purchases (before tax) is "+totalCost+". total Sales tax charged is "+ totalTax3 +". Total cost of the items is "+totalPaid3); 
    //print out the result 
    // the following code can be used to replace line 42 to 69
    //System.out.println("The Total cost of purchases for Pants (before tax)is "+costPants+". Sales tax charged buying Pants is "+ String.format("%.2f", taxPants)+". Total cost of Pants is "+String.format("%.2f", paidPants));
	  //System.out.println("The Total cost of purchases for Shirts (before tax) is "+costShirts+". Sales tax charged buying Pants is "+ String.format("%.2f", taxShirts) +". Total cost of Pants is "+ String.format("%.2f", paidShirts));
    //System.out.println("The Total cost of purchases for Pants (before tax) is "+costBelts+". Sales tax charged buying Pants is "+ String.format("%.2f", taxBelts) +". Total cost of Pants is "+ String.format("%.2f", paidBelts));
    //System.out.println("The Total cost of purchases (before tax) is "+totalCost+". total Sales tax charged is "+ String.format("%.2f", totalTax) +". Total cost of the items is "+String.format("%.2f", totalPaid));   
	}  //end of main method   
} //end of class