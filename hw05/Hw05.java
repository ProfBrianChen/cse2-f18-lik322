 //Poker is a game of chance. In poker, players draw sets of five playing cards, called hands. Different hands have different probabilities of occurring. The objective of this homework is to use while loops to calculate these probabilities.  We will limit our hands to those related to face values including: Four-of-a-kind, Three-of-a-kind, Two-Pair, and One-pair. 
import java.util.Scanner;
public class Hw05
{
  public static void main(String[] args) 
  {
    Scanner myScanner = new Scanner( System.in );
    System.out.print("How many times would you like to generate hands");
   
    while(myScanner.hasNextInt()==false)
    {
      System.out.print("error,you need input of integer\r\n");
      myScanner.next();
      System.out.print("How many times would you like to generate hands");
    }//give error when bad ints happens
    int loops = myScanner.nextInt();
    int k = 0;//count for amounts that is finished
    int Four = 0;
    int Three = 0;
    int Two = 0;
    int One = 0;
    //initilize numbers
    while(k<loops)
    {  
     int Num1 = (int)(Math.random()*52+1);
     int Num2 = (int)(Math.random()*52+1);
     int Num3 = (int)(Math.random()*52+1);
     int Num4 = (int)(Math.random()*52+1);
     int Num5 = (int)(Math.random()*52+1);
     //Num1-5 stands for number in the order of 52 with color
     int num1 = Num1%13+1;
     int num2 = Num2%13+1; 
     int num3 = Num3%13+1;
     int num4 = Num4%13+1;
     int num5 = Num5%13+1;
     //num 1-5 stands for number from 1-13 correspond to number without color
     if(Num1==Num2||Num1==Num3||Num1==Num4||Num1==Num5||Num2==Num3||Num2==Num4||Num2==Num5||Num3==Num4||Num3==Num5||Num4==Num5)
     {
     }
     //get rid of repeat the same card
     else
     {
       while (num1==num2&&num2==num3&&num4==num5||num1==num2&&num2==num4&&num3==num5||num1==num2&&num2==num5&&num3==num4||num1==num3&&num3==num4&&num2==num5||num1==num3&&num3==num5&&num2==num4||num1==num4&&num4==num5&&num2==num3||num2==num3&&num3==num4&&num1==num5||num2==num3&&num3==num5&&num1==num4||num2==num4&&num4==num5&&num1==num3||num3==num4&&num4==num5&&num1==num2)
      {
        One++;
        Two--;
        break;
      }
      while (num1==num2&&num2==num3&&num3==num4||num1==num2&&num2==num3&&num3==num5||num1==num2&&num2==num4&&num4==num5||num2==num3&&num3==num4&&num4==num5||num1==num3&&num3==num4&&num4==num5)
      {
        Four++;
        break;
      } 
      while (num1==num2&&num2==num3||num1==num2&&num2==num4||num1==num2&&num2==num5||num1==num3&&num3==num4||num1==num3&&num3==num5||num1==num4&&num4==num5||num2==num3&&num3==num4||num2==num3&&num3==num5||num2==num4&&num4==num5||num3==num4&&num4==num5)
      {
        Three++;
        break;
      }
      
      while (num1==num2&&num3==num4||num1==num2&&num3==num5||num1==num2&&num4==num5||num1==num3&&num2==num4||num1==num3&&num2==num5||num1==num3&&num4==num5||num1==num4&&num2==num3||num1==num4&&num2==num5||num1==num4&&num3==num5||num1==num5&&num2==num3||num1==num5&&num3==num4||num1==num5&&num4==num2||num2==num3&&num4==num5||num2==num4&&num3==num5||num2==num5&&num3==num4)
      {
        Two++;
        break;
      }     
      while (num1==num2||num1==num3||num1==num4||num1==num5||num2==num3||num2==num4||num2==num5||num3==num4||num3==num5||num4==num5)
      {
        One++;
        break;
      }
      //count the amount each case happens
      k++;  
     }    
    }
    int onefix = 1000*(One-Two-Three-Four)/k;//case Two Three Four are all included in case One
    int twofix =1000*(Two-Four)/k;//case Four is included in Case Two Also as the case full house we give a Two--when full house happens since it will be count twice
    int threefix =1000*(Three-Four)/k;//case Three is all included in case Four
    int fourfix =100000*Four/k;
    
    double onefix1 = (double)onefix/1000;
    double twofix1 = (double)twofix/1000;
    double threefix1= (double)threefix/1000;
    double fourfix1 = (double)fourfix/100000;
    
    System.out.println("The number of loops: (user inputted integer) "+ loops+"\r\n"+
                       "The probability of Four-of-a-kind: "+ fourfix1 +"\r\n"+
                       "The probability of Three-of-a-kind: "+threefix1 + "\r\n"+
                       "The probability of Two-pair: "+twofix1+"\r\n"+
                       "The probability of One-pair: "+onefix1+"\r\n"); 
  }  //end of main method   
} //end of class


