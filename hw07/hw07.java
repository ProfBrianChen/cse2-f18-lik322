//lingtao Kong CSE002
// use java to provide users with some tools
import java.util.Scanner;
public class hw07 
{
  public static void main( String args[] )
  {
	  String input ="";
    String empty ="";
    String method ="";
    input = sampleText(empty);
    method = printMenu(empty);
    if(method.equals("c")) 
    {
      getNumOfNonWSCharacters(input);
    }
    if(method.equals("w")) 
    {
      getNumOfWords( input);
    }
    if(method.equals("f")) 
    {
      findText(input);
    }
    if(method.equals("r")) 
    {
      replaceExclamation(input);
    }
    if(method.equals("s")) 
    {
      shortenSpace(input);
    }
  }
  //run different methods when the user choose the tool in the menu
  public static String sampleText(String a)
  {
    Scanner myScanner = new Scanner( System.in );
    System.out.println("Enter a sample text:");
    String text = myScanner.nextLine();
    System.out.println("your input"+ text);
    return text;
  } 
  //returm the tect
  public static String printMenu(String b)
  {
    String k = "";
    boolean check = true;
    while(check==true)
    {
    Scanner myScanner = new Scanner( System.in );
    System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println("Choose an option:");
    k = myScanner.nextLine();    
    if(k.equals("c")||k.equals("w")||k.equals("f")||k.equals("r")||k.equals("s"))
    {
      check=false;
      return k;  
    }  
      if(k.equals("q"))
    {
      break;
    }
    }
    return k;
  }
  //provide the users with a menu
   public static void getNumOfNonWSCharacters(String k)
  {
      int count = 0;
    for (int i=0; i<k.length(); i++) {
      if(('a' <= k.charAt(i))&&('z' >= k.charAt(i)) ||('A' <= k.charAt(i))&&('Z' >= k.charAt(i))) 
      {
      	count++;
    	}
      System.out.println("Number of non-whitespace characters:"+count);
  }  
  }
  //get the number of non ws character
   public static void getNumOfWords(String k)
  {
     int count =1;
     String w= "";
     w=k.replaceAll("\\s+"," ");
    for (int i=0; i<k.length(); i++) {
     if(' ' == k.charAt(i)) 
      {
      	count++;
    	}
     }
     System.out.println("Number of words: "+count);
   }
  //get the number of words
   public static void findText(String k)
  {
     System.out.println("Enter a word or phrase to be found:");
  }
  //find the times that a word appears
   public static void replaceExclamation(String k)
  {
    String i ="";
    i=k.replace('!','.');
     System.out.println("edited text: "+i);
  }
  //replace a word with another one
   public static void shortenSpace(String k)
  {
     String w= "";
     w=k.replaceAll("\\s+"," ");
     System.out.println("edited text"+w);
  }
  //shorten the space method reference https://blog.csdn.net/mingtianhaiyouwo/article/details/49851057
}
